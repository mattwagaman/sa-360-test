POETRY_RUN=poetry run

start:
	$(POETRY_RUN) python src/main.py

format:
	$(POETRY_RUN) black src/

lint:
	$(POETRY_RUN) flake8 src/

.PHONY: start format lint